import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;


public class ControleurSlider implements ChangeListener<Number>{
    private DemineurGraphique demineur;

    public ControleurSlider(DemineurGraphique demineur){
        this.demineur = demineur;

    }
    
    @Override
    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
    {   
        Double nouvelleValeur = (Double) newValue;
        this.demineur.setLabel(nouvelleValeur);
    }
}

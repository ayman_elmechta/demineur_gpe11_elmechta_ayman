import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
/**
 * Tests des méthodes de Grille
 */
public class TestGrille {
    
    private Grille grille;

    @Before
    public void initialisation_depenses(){
        this.grille = new Grille(5,5,5);
    }
    @Test
    public void testcréationPlateau(){
        int nbrCase = 0;
        for(List<Case> ligne : this.grille.getGrille()){
            for(Case _case : ligne){
                nbrCase++;
            }
        }
        assertEquals(25, nbrCase);

    }
    @Test
    public void testposerBombe(){

        assertEquals(5, this.grille.getNombreDeBombes());
    }

    @Test
    public void testGetNbrMarquee(){
        this.grille.getCase(1,1).marquer();
        assertEquals(1, this.grille.getNombreDeCasesMarquees());
        this.grille.getCase(2,1).marquer();
        assertEquals(2, this.grille.getNombreDeCasesMarquees());
    }

    @Test
    public void testEstPerdu(){
        assertTrue(!this.grille.estPerdu());
        this.grille.getCase(1,1).reveler();
        this.grille.getCase(1,1).ajouteBombe();
        assertTrue(this.grille.estPerdu());
    }
    @Test
    public void estGagnee(){
        for(List<Case> ligne : this.grille.getGrille()){
            for(Case _case : ligne){
                if(!_case.estBombe()){
                    _case.reveler();
                }
            }
        }
        assertTrue(this.grille.estGagnee());
    }

    @Test
    public void testDecouvreToutesLesCases(){
        this.grille.decouvreToutesLesCases();
        for(List<Case> ligne : this.grille.getGrille()){
            for(Case _case : ligne){
                assertTrue(_case.estRevelee());
            }
        }
    }

    public void testDecouvreCase(){
        
        this.grille.decouvreCase(this.grille.getCase(1, 1));
        assertTrue(this.grille.getCase(1, 1).estRevelee());
        
    }


}

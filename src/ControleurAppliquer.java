import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;


/**
 * Contrôleur à activer lorsque l'on clique sur le bouton appliquer
 */
public class ControleurAppliquer implements EventHandler<ActionEvent> {


    private DemineurGraphique demineur;
    /**
     * @param demineur modèle du jeu
     * 
     */

    public ControleurAppliquer( DemineurGraphique demineur) {
        this.demineur = demineur;
    }
    
    @Override
    public void handle(ActionEvent e){
        
        try{
            int ligne = Integer.parseInt(this.demineur.getLigne());
            int colonne = Integer.parseInt(this.demineur.getColonne());
            int bombe = Integer.parseInt(this.demineur.getNbrBombe());
            if(verifCoup(ligne, colonne, bombe, e));
                this.demineur.setElement(colonne, ligne, bombe);
                this.demineur.rejouer();
        }
        catch(NumberFormatException erreur){
            this.demineur.setErreur("Les informations données \nsont incorrectes");
        }
        catch(TropDeBombes tb){
            this.demineur.setErreur("Il y a trop de bombes sur le plateau !");
        } catch (TropDeLigne e1) {
            this.demineur.setErreur("Il y a trop de lignes !");
        } catch (TropDeColonne e1) {
            this.demineur.setErreur("Il y a trop de colonnes !");
        } catch (PasAssezColonne e1) {
            this.demineur.setErreur("Il n'y a pas assez de colonnes !");
        } catch (PasAssezLigne e1) {
            this.demineur.setErreur("Il n'y a pas assez de lignes !");
        } catch (EntierNegatif e1) {
            this.demineur.setErreur("Les informations doivent être des nombres entiers !");
        }
        

    }

    public boolean verifCoup(int ligneCoup, int colonneCoup, int nbBombe, ActionEvent e) throws TropDeBombes,TropDeLigne,TropDeColonne,PasAssezColonne,PasAssezLigne,EntierNegatif{
        if( (float) nbBombe / (ligneCoup*colonneCoup) > 0.4){
            throw new TropDeBombes();
        }
        else if(colonneCoup > 20){

            throw new TropDeColonne();
        }
        else if(ligneCoup > 20){
            throw new TropDeLigne();
        }      
        else if(colonneCoup < 5){
            throw new PasAssezColonne();
        }
        else if(ligneCoup < 5){
            throw new PasAssezLigne();
        }        
        else if(ligneCoup < 0 || colonneCoup < 0 || nbBombe < 0){
            throw new EntierNegatif();
        }
        return true;
    }
}

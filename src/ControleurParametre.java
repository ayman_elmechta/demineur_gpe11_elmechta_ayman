
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer ou Lancer une partie
 */
public class ControleurParametre implements EventHandler<ActionEvent> {
    /**
     * modèle du jeu
     */
    private DemineurGraphique demineur;
    /**
     * vue du jeu
     **/

    /**
     * @param demineur modèle du jeu
     
     */
    public ControleurParametre(DemineurGraphique demineur) {
        this.demineur = demineur;
    }

    /**
     * L'action consiste à recommencer une partie. Il faut vérifier qu'il n'y a pas une partie en cours
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        if(this.demineur.getPlateau().getNombreDeCasesRevelees() != 0){
            Optional<ButtonType> reponse = this.demineur.popUpPartieEnCours().showAndWait(); // on lance la fenêtre popup et on attends la réponse
            // si la réponse est oui
            if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                this.demineur.modeParametres();
        }
        }else{
            this.demineur.modeParametres();}
        


        
    }
}

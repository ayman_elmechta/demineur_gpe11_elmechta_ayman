import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
/**
 * Tests des méthodes de Case
 */
public class TestCase {
    
    private Case uneCase;

    @Before
    public void initialisation_case(){
        this.uneCase = new Case();
    }
    @Test
    public void testCreationCase(){
        assertTrue(!this.uneCase.estRevelee());
        assertTrue(!this.uneCase.estMarquee());
        assertTrue(!this.uneCase.estBombe());
    }
    @Test
    public void testMettreBombe(){
        this.uneCase.ajouteBombe();
        assertTrue(this.uneCase.estBombe());
        Case uneCaseSansBombe = new Case();
        assertTrue(!uneCaseSansBombe.estBombe());

    }
    @Test
    public void testEstRevele(){
        assertTrue(!this.uneCase.estRevelee());
        this.uneCase.reveler();
        assertTrue(this.uneCase.estRevelee());

    }

    @Test
    public void testEstMarque(){
        assertTrue(!this.uneCase.estMarquee());
        this.uneCase.marquer();
        assertTrue(this.uneCase.estMarquee());
        

    }
    @Test
    public void testCaseVoisine(){
        assertTrue(this.uneCase.getCasesVoisines().isEmpty());
        this.uneCase.ajouteCaseVoisine(new Case());
        assertTrue(!this.uneCase.getCasesVoisines().isEmpty());
    }
    @Test
    public void testGetNbBombesVoisines(){
        assertEquals(0, this.uneCase.getNbBombesVoisines());
        Case uneCaseAvecBombe = new Case();
        this.uneCase.ajouteCaseVoisine(uneCaseAvecBombe);
        assertEquals(1, this.uneCase.getNbBombesVoisines());
        
    }


}

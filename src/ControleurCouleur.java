import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ColorPicker;
import javafx.scene.shape.Circle;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton info
 */
public class ControleurCouleur implements EventHandler<ActionEvent> {

    private DemineurGraphique demineur;
    private Circle circle;
    private ColorPicker colorPicker;

    /**
     * @param p vue du jeu
     */
    public ControleurCouleur(DemineurGraphique demineur, Circle circle, ColorPicker colorPicker) {
        this.demineur = demineur;
        this.circle = circle;
        this.colorPicker = colorPicker;
    }

    /**
     * L'action consiste à afficher une fenêtre popup précisant les règles du jeu.
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent event) {
        circle.setFill(colorPicker.getValue());
        this.demineur.setCouleur(colorPicker.getValue().toString());
    }
}


import javafx.event.EventHandler; 
import javafx.scene.input.MouseEvent; 
import javafx.scene.input.MouseButton;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import java.util.Optional;
/**
 * Contrôleur à activer lorsque l'on clique sur un bouton de la grille
 */
public class ControleurBouton implements EventHandler<MouseEvent>{
    
    private Bouton bouton;
    private Case laCase;
    private DemineurGraphique demineur;
    private Grille lePlateau;
    
    public ControleurBouton(Bouton bouton, Case laCase, DemineurGraphique demineur, Grille lePlateau){
        this.bouton = bouton;
        this.laCase = laCase;
        this.demineur = demineur;
        this.lePlateau = lePlateau;
    }
    
    
    /** 
     * Gère le clique droit/gauche sur l'un des boutons du jeu
     * @param e L'evenement de l'action
     * 
     */
    @Override
    public void handle(MouseEvent e) {
        if (!this.laCase.estRevelee()){
            if (e.getButton() == MouseButton.PRIMARY){
            System.out.println("clic gauche");
            if(this.lePlateau.getNombreDeCasesRevelees() ==0 ){
                    while(!(this.laCase.getNbBombesVoisines() == 0)){
                        this.lePlateau.regenerePlateau(laCase);
                        this.bouton.maj();        
                        this.demineur.maj_de_la_grille();        
                        this.bouton.setDisable(true);
                    }
                }
                this.lePlateau.decouvreCase(this.laCase);
                this.bouton.maj();        
                this.demineur.maj_de_la_grille();        
                this.bouton.setDisable(true);
            }
               
            
            if(e.getButton() == MouseButton.SECONDARY){
                System.out.println("clic droit");
                if (!this.laCase.estRevelee()){
                    this.laCase.marquer();
                    this.bouton.maj();
                }
            }
        }
        
        
        this.demineur.maj_des_infos();

        if (this.lePlateau.estPerdu()){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Vous avez perdu !\nVoulez-vous rejouer ?",ButtonType.YES, ButtonType.NO);
            alert.setTitle("Attention");
            this.lePlateau.decouvreToutesLesCasesBombes();
            this.demineur.maj_de_la_grille();
            Optional<ButtonType> rep = alert.showAndWait();

            if (rep.isPresent() && rep.get()==ButtonType.YES){
                this.demineur.rejouer();
                this.demineur.maj_des_infos();
            }
            else{
             
                this.demineur.desactiver();
            }            
        }
        if(this.lePlateau.estGagnee()){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Vous avez gagné !\nVoulez-vous rejouer ?",ButtonType.YES, ButtonType.NO);
            alert.setTitle("Bravo !");
            this.lePlateau.decouvreToutesLesCasesBombes();
            this.demineur.maj_de_la_grille();
            Optional<ButtonType> rep = alert.showAndWait();

            if (rep.isPresent() && rep.get()==ButtonType.YES){
                this.demineur.rejouer();
                this.demineur.maj_des_infos();
            }
            else{
                this.lePlateau.decouvreToutesLesCasesBombes();
                this.demineur.desactiver();
            }            
        }
        
    }
}

import java.util.ArrayList;
import java.util.List;

public class Case {
    

    private List<Case> caseVoisine;
    private boolean estBombe;
    private boolean estRevelee;
    private boolean estMarquee;

    public Case(){
        this.caseVoisine = new ArrayList<>();
        this.estBombe = false;
        this.estRevelee = false;
        this.estMarquee = false;
    }
    /** 
    * Fonction qui pose une bombe sur cette case
    * et renvoie true si l'opération s'est bien passé, false sinon
    * @return boolean
    */
    public boolean ajouteBombe(){
        if(this.estBombe){
            return false;
        }
        this.estBombe = true;
        return true;
    }

    /** 
    * Renvoie true si une bombe est posé 
    * sur cette case et false sinon.
    * @return boolean
    */
    public boolean estBombe(){
        return this.estBombe;
    }

    /** 
    * Renvoie true si la case est découverte 
    * et false sinon.
    * @return boolean
    */
    public boolean estRevelee(){
        return this.estRevelee;
    }
    /** 
    * Renvoie true si la case est marqué 
    * et false sinon.
    * @return boolean
    */
    public boolean estMarquee(){
        return this.estMarquee;
    }

    /** 
    * Révèle la case 
    */
    public void reveler(){
        this.estMarquee = false;
        this.estRevelee = true;
    }
    /** 
    * Marque la case 
    */
    public void marquer(){
        this.estMarquee = !this.estMarquee;
    }
    /** 
    * Renvoie la liste des cases voisines 
    * @return List<Case>   
    */
    public List<Case> getCasesVoisines(){
        return this.caseVoisine;
    }
    /** 
    * Ajoute une case voisine à la liste des case vosines 
    de la case passé en paramètre
    * @param (Case) uneCase
    */
    public void ajouteCaseVoisine(Case uneCase){
        this.caseVoisine.add(uneCase);
    }
    /** 
     * Retourne le nombre de bombes voisines à cette case
    * @return int
    */
    public int getNbBombesVoisines(){
        int nbBombes = 0;
        for(Case uneCase : this.caseVoisine){
            if(uneCase.estBombe()){
                nbBombes++;
            }
        }
        return nbBombes;
    }
    /** 
     * Retourne l'affichage du plateau dans la console
     * @return String
     */
    public String getAffichage(){

        if(!this.estMarquee && !this.estRevelee){
            return "  ";
        }
        else if(this.estRevelee && this.estBombe){
            return "@";
        }
        else if(this.estMarquee){
            return " ?";
        }
        else{
                return String.valueOf(getNbBombesVoisines());
            }
  
        }

    
        
    /** 
     * @return String
     */
    @Override
    public String toString(){
        return this.getAffichage();
    }
    public void retireBombe() {
        this.estBombe = false;
    }
    public void retireCaseVoisine() {
        this.caseVoisine.clear();
    }

}

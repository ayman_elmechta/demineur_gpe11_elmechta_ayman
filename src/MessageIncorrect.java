public class MessageIncorrect extends Exception {

    private int pos;

    public MessageIncorrect(){
        this.pos = -1;
    }

    public MessageIncorrect(int position){
        this.pos = position;
    }

    public int getPosition(){return this.pos;}
    
}
